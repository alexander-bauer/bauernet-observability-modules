# -*- coding: utf-8 -*-
# Spheode / Sphinx config

project = "Bauernet Observability Modules"
author = "Alexander Bauer"
copyright = copyright_since(author, '2023')

highlight_language = "terraform"
mermaid_output_format = "raw"

terraform_sources = "../"
