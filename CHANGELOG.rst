#########
Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
============

Changed
-------

* Upgraded to using new OAuth2-Proxy role-based access

Fixed
-----

* Removed unused version requirements

[0.3.0] - 2023-06-30
====================

Added
-----

* Parameter for ``prometheus_scrape`` configuration to set the interval and timeout

[0.2.1] - 2023-06-30
====================

Added
-----

* Debug logging flag for OpenTelemetry

[0.2.0] - 2023-06-30
====================

Added
-----

* Added top-level module which configures complete metrics gathering stack using the
  OpenTelemetry Collector and OpenObserve.
* Added README
* Sync gitignore

Changed
-------

* Updated required versions (Terraform -> 1.4.6)
* Synchronized pre-commit config

[0.1.0] - 2023-04-30
====================

Added
-----

* Initial set of modules
