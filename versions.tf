terraform {
  required_version = ">= 1.4.6"

  required_providers {
    random = {
      source  = "hashicorp/random"
      version = ">= 3.5.1"
    }

    external = {
      source  = "hashicorp/external"
      version = ">= 2.3.1"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.15.0"
    }

    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.7.1"
    }

    keycloak = {
      source = "mrparkers/keycloak"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}
