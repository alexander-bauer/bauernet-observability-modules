resource "kubernetes_namespace" "namespace" {
  metadata {
    name = var.namespace
  }
}

locals {
  namespace = one(kubernetes_namespace.namespace.metadata).name
}
