variable "namespace" {
  description = "Namespace to create for observability tools"
  type        = string
  default     = "observability"
}

variable "dash_fqdn" {
  description = "FQDN on which to place the Elastic web ingress"
  type        = string
}

variable "dash_ingress_annotations" {
  description = "Annotations to add to the ingress"
  type        = map(string)
  default     = {}
}

variable "storage_size" {
  description = "Size for OpenObserve storage space"
  type        = string
  default     = null
}

variable "opentelemtry_collector_debug" {
  description = "Whether to enable debug logging in the OpenTelemetry Collector"
  type        = bool
  default     = null
}

variable "prometheus_scrape" {
  description = "Configuration for the Prometheus scraping process"
  type = object({
    interval = string
    timeout  = string
  })
  default = {
    interval = "60s"
    timeout  = "10s"
  }
}
