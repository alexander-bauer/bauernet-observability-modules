variable "namespace" {
  description = "Namespace into which to install resources"
  type        = string
}

variable "chart" {
  description = "Helm chart to install"
  type = object({
    repository = string
    chart      = string
  })
  default = {
    repository = "https://open-telemetry.github.io/opentelemetry-helm-charts"
    chart      = "opentelemetry-collector"
  }
}

variable "chart_version" {
  description = "Particular chart version to install; latest if not specified"
  type        = string
  default     = null
}

variable "exporters" {
  description = "Exporters configuration for OTel Collector"
  type        = map(any)
}

variable "debug" {
  description = "Whether to enable debug logging in the OpenTelemetry Collector"
  type        = bool
  default     = false
}

variable "prometheus_scrape" {
  description = "Configuration for the Prometheus scraping process"
  type = object({
    interval = string
    timeout  = string
  })
  default = {
    interval = "60s"
    timeout  = "10s"
  }
}
