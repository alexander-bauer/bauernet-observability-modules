locals {
  scrape_config_base_path = "${path.module}/prometheus-scrape-configs"
}

resource "helm_release" "otel" {
  namespace  = var.namespace
  name       = "otel"
  repository = var.chart.repository
  chart      = var.chart.chart
  version    = var.chart_version

  values = [
    # Break exporters out into its own block, because it's sensitive.
    yamlencode({
      config = {
        exporters = var.exporters
      }
    }),
    yamlencode({
      mode = "daemonset"
      presets = {
        #logsCollection       = { enabled = true }
        hostMetrics          = { enabled = true }
        kubernetesAttributes = { enabled = true }
        kubernetesEvents     = { enabled = true }
        #clusterMetrics       = { enabled = true }
        kubeletMetrics = { enabled = true }
      }
      config = {
        processors = {
          batch = {}
        }
        service = {
          telemetry = {
            logs = {
              level = var.debug == true ? "debug" : "info"
            }
          }
          pipelines = {
            metrics = {
              exporters = keys(var.exporters)
            }
          }
        }
        receivers = {
          prometheus = {
            config = {
              global = {
                scrape_interval = var.prometheus_scrape.interval
                scrape_timeout  = var.prometheus_scrape.timeout
              }
              # Load scrape configs from the source directory.
              scrape_configs = [
                for f in fileset(local.scrape_config_base_path, "*.yml") :
                yamldecode(file("${local.scrape_config_base_path}/${f}"))
              ]
            }
          }
        }
      }
      clusterRole = {
        rules = [
          {
            apiGroups = ["apps"]
            resources = ["replicasets"]
            verbs     = ["get", "list", "watch"]
          },
          {
            apiGroups = [""]
            resources = ["endpoints", "services"]
            verbs     = ["get", "list", "watch"]
          }
        ]
      }
  })]
}
