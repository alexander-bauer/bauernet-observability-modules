#module "keycloak-client" {
#  source = "https://gitlab.com/alexander-bauer/bauernet-authzn-modules//keycloak/client?ref=v0.4.0"
#
#  realm         = var.realm
#  client_id     = var.client_id
#  client_name   = var.client_name
#  redirect_uris = ["https://grafana.bauernet.org/*"]
#  roles = {
#    "${var.client_id}-admin" = {
#      description = "Administrators for ${var.client_name}"
#      groups      = var.admin_groups
#    }
#    "${var.client_id}-users" = {
#      description = "Read only users for ${var.client_name}"
#      groups      = var.user_groups
#    }
#  }
#}

module "openobserve" {
  source = "./openobserve-module"

  namespace = local.namespace
  fqdn      = var.dash_fqdn
  ingress_annotations = merge(
    var.dash_ingress_annotations,
    module.auth.annotations.nginx.role.openobserve-fullaccess
  )

  #replicas     = var.dash_replicas
  storage_size = var.storage_size
}

module "otel" {
  source    = "./opentelemetry"
  namespace = local.namespace

  exporters = {
    "prometheusremotewrite" = {
      endpoint = module.openobserve.ingest.endpoints.prometheus
      headers = {
        Authorization = module.openobserve.ingest.credentials.header
      }
    }
  }
  prometheus_scrape = var.prometheus_scrape
  debug             = var.opentelemtry_collector_debug
}
