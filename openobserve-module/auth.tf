locals {
  root_email = "root@${var.fqdn}"

  basic_auth  = "${local.root_email}:${random_password.root_password.result}"
  auth_header = "Basic ${base64encode(local.basic_auth)}"
  auto_auth_annotations = {
    # Supply basic auth automatically.
    "nginx.ingress.kubernetes.io/configuration-snippet" = <<-EOF
      proxy_set_header Authorization "${local.auth_header}";
    EOF
  }
}

resource "random_password" "root_password" {
  length  = 32
  special = false
}
