locals {
  service_name = "service/openobserve"
  service_port = 80
  service_url  = "http://openobserve.${var.namespace}.svc"
}

# Use kubectl port-forward and curl to read the ingest passcode.
data "external" "openobserve-ingestkey" {
  depends_on = [helm_release.openobserve]

  program = ["${path.module}/openobserve-ingestkey.sh"]
  query = {
    email        = local.root_email
    password     = random_password.root_password.result
    fw_namespace = var.namespace
    fw_target    = local.service_name
    fw_port      = local.service_port
  }
}

locals {
  ingest_user = data.external.openobserve-ingestkey.result.user
  ingest_pass = data.external.openobserve-ingestkey.result.passcode
}

output "ingest" {
  description = "Credentials and endpoints for data ingestion"
  sensitive   = true
  value = {
    credentials = {
      username = local.ingest_user
      password = local.ingest_pass
      header = join(" ", [
        "Basic",
        base64encode("${local.ingest_user}:${local.ingest_pass}")
      ])
    }
    endpoints = {
      prometheus = "${local.service_url}/api/default/prometheus/api/v1/write"
      json       = "${local.service_url}/api/default/default/_json"
    }
  }
}
