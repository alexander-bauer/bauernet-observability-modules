variable "namespace" {
  description = "Existing namespace to create resources in"
  type        = string
}

variable "chart" {
  description = "Helm chart to install"
  type = object({
    repository = string
    chart      = string
  })
  default = {
    repository = "https://mshade.github.io/openobserve-chart"
    chart      = "openobserve"
  }
}

variable "chart_version" {
  description = "Particular chart version to install; latest if not specified"
  type        = string
  default     = null
}

variable "fqdn" {
  description = "FQDN on which to place the ingress"
  type        = string
}

variable "ingress_annotations" {
  description = "Annotations to add to the ingress"
  type        = map(string)
  default     = {}
}

variable "storage_size" {
  description = "Size of PVC"
  type        = string
  default     = "10Gi"
}
