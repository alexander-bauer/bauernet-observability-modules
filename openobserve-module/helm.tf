resource "helm_release" "openobserve" {
  namespace  = var.namespace
  name       = "openobserve"
  repository = var.chart.repository
  chart      = var.chart.chart
  version    = var.chart_version

  values = [
    yamlencode({ # separated sensitive values
      auth = {
        initialPassword = random_password.root_password.result
        rootUserEmail   = local.root_email
      }
    }),
    yamlencode({
      ingress = {
        enabled = true
        annotations = merge(
          local.auto_auth_annotations,
          var.ingress_annotations
        )
        hosts = [{
          host  = var.fqdn
          paths = [{ path = "/", pathType = "ImplementationSpecific" }]
        }]
      }
      storage = {
        volumeSize = var.storage_size
      }
    }),
  ]
}

output "password" {
  sensitive = true
  value     = random_password.root_password.result
}
