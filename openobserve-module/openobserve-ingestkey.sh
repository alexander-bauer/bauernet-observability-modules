#!/bin/bash

set -ex

eval "$(jq -r '@sh "EMAIL=\(.email) PASSWORD=\(.password) FW_NAMESPACE=\(.fw_namespace) FW_TARGET=\(.fw_target) FW_PORT=\(.fw_port)"')"

# Make sure background processes are killed on exit
trap "jobs -p | xargs -r kill" SIGINT SIGTERM EXIT

# Start kubectl port-forward in the background
kubectl port-forward -n "$FW_NAMESPACE" "$FW_TARGET" "8080:$FW_PORT" 2>/dev/null 1>&2 &
fw_pid=$!

# Wait for establish
#sleep 1

# Check on port-forward status. If it's still running, continue.
if ! kill -0 $fw_pid; then
  echo "Port-forward died" 1>&2
  exit 1
fi

RESULT=$(curl --retry 2 --retry-max-time 5 --retry-all-errors \
  -X GET -H "Accept: application/json" -u "$EMAIL:$PASSWORD" \
  "http://localhost:8080/api/1/organizations/passcode")

echo "$RESULT" | jq '.data'
