##############################
Bauernet Observability Modules
##############################

Observability is a big topic. See the `OpenTelemetry primer on Observability
<https://opentelemetry.io/docs/concepts/observability-primer/#what-is-observability>`_
for an introduction.

.. toctree::
   :maxdepth: 1
   :glob:

   changelog

Troubleshooting
===============

How-to Inspect the ``grafana.ini``
----------------------------------

Use this one-liner to dump the contents of ``grafana.ini`` as it is read by the Grafana
pod.

.. code-block:: console

   $ kubectl -n observability get configmap grafana -o json | jq -r '.data["grafana.ini"]'
